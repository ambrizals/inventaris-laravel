<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMerekRequest;
use App\Http\Controllers\Controller;
use App\merek;

use Illuminate\Support\Facades\Request;

// use Illuminate\Http\Request;

class MerekController extends Controller
{
    public function index(){
    	$merek = Merek::latest('created_at')->get();
    	$merek = Merek::where('flag_delete',0)->paginate(10);
    	return view('merek.index', compact('merek'));
    }
    public function archive(){
    	$merek = Merek::latest('created_at')->get();
    	$merek = Merek::where('flag_delete',1)->paginate(10);
    	return view('merek.deleted', compact('merek'));
    }
    public function store(CreateMerekRequest $request){
        Merek::create($request->all());
        return redirect('merek')->with('pesan', 'Data berhasil ditambahkan!');
    }
    public function destroy($id){
		Merek::where('id',$id)->update(['flag_delete' => 1]);
        return redirect('merek')->with('pesan', 'Data berhasil dihapus!');
    }
    public function restore($id){
		Merek::where('id',$id)->update(['flag_delete' => 0]);
        return redirect('merek')->with('pesan', 'Data berhasil dikembalikan!');
    }
    public function edit($id){
    	$merek = Merek::find($id);
    	return view('merek.edit',compact('merek'));
    }
    public function update($id){
        $merek = Merek::find($id);
        $merek->update(Request::all());
        return redirect('merek')->with('pesan', 'Data berhasil diubah!');
    }
}
