<!DOCTYPE html>
<html lang="en">
    <head>
        <title>@yield('title')</title>
        {{ Html::style('css/main.css') }}
        {{ Html::style('css/bootstrap.min.css') }}
        <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
        {{ Html::script('js/bootstrap.js') }}
    </head>
        <body>
            @include('include.header')
            <!-- /.navbar-collapse -->
            <!-- Page Heading -->
            @yield('content')
            <!-- /.row --> 
            @include('include.footer')
        </body>
</html>