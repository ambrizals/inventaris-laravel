<header>
    <nav class="navbar navbar-inverse navbar-static-top nav-add">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('') }}">Inventaris Stiki</a>
                </div>

                <div class="collapse navbar-collapse" id="collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="{{ url('') }}">Beranda</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Merek <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ URL('merek') }}">Daftar Merek</a></li>
                            <li><a href="{{ URL('merek/archive') }}">Merek Yang Terhapus</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ url('#') }}">Tentang</a></li>
                </ul>
                <div class="nav navbar-form navbar-right">
                    <a href="#" class="btn btn-primary">Logout</a>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>