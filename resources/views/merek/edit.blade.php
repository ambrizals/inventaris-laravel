@extends('layouts.master')
@section('title', 'Ubah Merek - Inventaris Stiki')
@section('content')
<div class="container edit">
	<div class="panel panel-primary">
		<div class="panel-heading">
			Ubah Merek : {{ $merek->nama_merek }}
		</div>
		{!! Form::model($merek, ['route' => ['Merek Update', $merek->id], 'method' => 'PUT']) !!}
		<div class="panel-body">
	        <div class="form-group">
	            {!! Form::label('nama_merek', 'Nama Merek') !!}
	            {!! Form::text('nama_merek', null, ['class' => 'form-control', 'placeholder' => 'masukan nama']) !!}
	        </div>
	        {!! Form::submit('Edit Data', ['class' => 'btn btn-primary tombol-panjang']) !!}
	        {!! Form::close() !!}
	    </div>
	</div>
</div>
@stop