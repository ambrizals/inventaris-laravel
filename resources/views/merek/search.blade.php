@extends('layouts.master')
@section('title', 'Merek - Inventaris Stiki')
@section('content')
<div class="container">
	<div class="page-header">
		<h1>Daftar Merek</h1>
		<p>Berikut adalah daftar merek yang terdaftar.</p>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="tombol-tambah">
				<a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-merek">Tambah Merek Baru</a>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Navigasi Merek
				</div>
				<div class="list-group">
					<a href="{{ URL('merek') }}" class="list-group-item">Daftar Merek</a>
					<a href="{{ URL('merek/deleted') }}" class="list-group-item">Merek Yang Terhapus</a>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-primary">
				<div class="panel-heading">
					List
				</div>
				<table class="table table-responsive table-modified">
					<thead>
						<tr>
							<td class="col-md-1">ID</td>
							<td class="col-md-6">Nama</td>
							<td class="col-md-3">Aksi</td>
						</tr>
					</thead>
					<tbody>
						@foreach ($merek as $key => $value)
						<tr>
							<td>{{ $value->id }}</td>
							<td>{{ $value->nama_merek }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tengah">
				{{ $merek->links() }}
			</div>
		</div>
	</div>
</div>
@stop