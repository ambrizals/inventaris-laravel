@extends('layouts.master')
@section('title', 'Merek - Inventaris Stiki')
@section('content')
<div class="container">
	<div class="page-header">
		<h1>Daftar Merek</h1>
		<p>Berikut adalah daftar merek yang terdaftar.</p>
	</div>
	@If (Session::has('pesan'))
	<div class="alert alert-success">
		{{ Session::get('pesan') }}
	</div>
	@Endif
	@if ($errors->any())
	<div class="alert alert-warning">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif
	<div class="row">
		<div class="col-md-3">
			<div class="tombol-tambah">
				<a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-merek">Tambah Merek Baru</a>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Navigasi Merek
				</div>
				<div class="list-group">
					<a href="{{ URL('merek') }}" class="list-group-item">Daftar Merek</a>
					<a href="{{ URL('merek/archive') }}" class="list-group-item">Merek Yang Terhapus</a>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-primary">
				<div class="panel-heading">
					List
				</div>
				<table class="table table-responsive table-modified">
					<thead>
						<tr>
							<td class="col-md-1">ID</td>
							<td class="col-md-6">Nama</td>
							<td class="col-md-3">Aksi</td>
						</tr>
					</thead>
					<tbody>
						@foreach ($merek as $key => $value)
						<tr>
							<td>{{ $value->id }}</td>
							<td>{{ $value->nama_merek }}</td>
							<td>
								<a href="{{ URL( 'merek/'. $value->id . '/edit') }}" class="tombol-edit btn btn-primary">Edit</a>
		                        {!! Form::open(['url' => 'merek/'.$value->id, 'class' => 'pull-left tombol-hapus']) !!}
		                        {!! Form::hidden('_method', 'DELETE') !!}
		                        {!! Form::submit('Hapus Data', ['class' => 'btn btn-danger']) !!}
		                        {!! Form::close() !!}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="tengah">
				{{ $merek->links() }}
			</div>
		</div>
	</div>
</div>
<!-- Modal Buat Merek Baru -->
<div class="modal fade" id="tambah-merek" tabindex="-1" role="dialog" aria-labelledby="tambah-merek">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Tambah Merek Baru</h4>
			</div>
			{!! Form::open(['url'=>'merek']) !!}
			<div class="modal-body">
		        <div class="form-group">
		            {!! Form::label('nama_merek', 'Nama Merek') !!}
		            {!! Form::text('nama_merek', null, ['class' => 'form-control', 'placeholder' => 'Masukkan nama merek']) !!}
		        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit('Tambah Merek', ['class' => 'btn btn-primary']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop