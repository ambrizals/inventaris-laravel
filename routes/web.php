<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::resource('/merek','MerekController');
Route::get('','HomeController@index')->name('Halaman Utama');
Route::prefix('merek')->group(function () {
	Route::get('', 'MerekController@index')->name('Halaman Index');
	Route::post('', 'MerekController@store')->name('Merek Submit');
	Route::delete('{merek}', 'MerekController@destroy')->name('Merek Hapus');
	Route::get('{merek}/edit', 'MerekController@edit')->name('Merek Edit');
	Route::put('{merek}', 'MerekController@update')->name('Merek Update');
	Route::get('archive','MerekController@archive')->name('Merek Arsip');
	Route::delete('{merek}/restore', 'MerekController@restore')->name('Merek Restore');
});